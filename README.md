# Swatch To Style

An Inkscape 1.1+ extension 

▶ Converts element fill and stroke
  from Swatch to style

▶ Appears in 'Extensions>Colour'

▶ Only checks inline styles, not class or element attributes

