#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Swatch to Style - converts element fill and stroke from Swatch to style
# Only checks inline styles, not class or element attributes
# An Inkscape 1.1+ extension
##############################################################################

import inkex

from copy import deepcopy
import os
import time



global swatch_list
swatch_list = []

def export_svg(self, svg):

    csv_name = self.options.export_svg_filepath.split('.')[0]

    try:
        try_file = open('test_file12345', 'w')
        try_file.close()
        os.remove('test_file12345')
    except:
        inkex.errormsg('filepath is not valid ( or non writeable ) ')
        return

    filename_suffix = (str(time.time())).replace('.', '')

    with open(f'{csv_name}_{filename_suffix}.svg', 'w') as svg_file:
        svg_file.seek(0)
        svg_text = svg.tostring().decode('utf-8')
        svg_file.write(svg_text)
        # inkex.errormsg(svg_text)

def remove_swatch_defs(self, svg):
    global swatch_list

    for swatch in swatch_list:

        swatch.delete()



def swatch_to_style(self, svg, element_list):

    for element in element_list:
        if 'fill' in element.style:
            element_fill = element.style['fill']
            if 'url' in element_fill:
                element_fill_id = element_fill.split('#')[1].replace(')', '')


                fill_element = svg.getElementById(element_fill_id)

                # # Need to check if this fill is real or an xlink to another fill
                xlink_value = (fill_element.get('{http://www.w3.org/1999/xlink}href'))

                if xlink_value != None:

                    xlink_id = fill_element.get('{http://www.w3.org/1999/xlink}href').split('#')[1].replace(')', '')

                    fill_element = svg.getElementById(xlink_id)

                if fill_element.get(r'{http://www.inkscape.org/namespaces/inkscape}swatch'):

                    gradient_stop = fill_element.xpath('./svg:stop')
                    stop_color = gradient_stop[0].style.get('stop-color')
                    stop_opacity = gradient_stop[0].style.get('stop-opacity')
                    if stop_color != 'None':
                        element.style['fill'] = stop_color
                        if self.options.opacity_cb == 'true':
                            element.style['fill-opacity'] = stop_opacity
                        swatch_list.append(fill_element)

        if 'stroke' in element.style:
            element_stroke = element.style['stroke']
            if 'url' in element_stroke:
                element_stroke_id = element_stroke.split('#')[1].replace(')', '')
                stroke_element = svg.getElementById(element_stroke_id)

                # # Need to check if this stroke is real or an xlink to another stroke
                xlink_value = (stroke_element.get('{http://www.w3.org/1999/xlink}href'))

                if xlink_value != None:

                    xlink_id = stroke_element.get('{http://www.w3.org/1999/xlink}href').split('#')[1].replace(')', '')

                    stroke_element = svg.getElementById(xlink_id)

                if stroke_element.get(r'{http://www.inkscape.org/namespaces/inkscape}swatch'):

                    gradient_stop = stroke_element.xpath('./svg:stop')
                    stop_color = gradient_stop[0].style.get('stop-color')
                    if self.options.opacity_cb == 'true':
                        stop_opacity = gradient_stop[0].style.get('stop-opacity')
                    element.style['stroke-opacity'] = stop_opacity
                    if stop_color != 'None':
                        element.style['stroke'] = stop_color
                        swatch_list.append(stroke_element)

    return svg

class SwatchToStyle(inkex.EffectExtension):


    def add_arguments(self, pars):

        pars.add_argument("--swatch_to_style_notebook", type=str, dest="swatch_to_style_notebook", default=0)

        pars.add_argument("--export_svg_filepath", type=str, dest="export_svg_filepath", default=None)

        pars.add_argument("--opacity_cb", type=str, dest="opacity_cb")

        pars.add_argument("--delete_swatches_cb", type=str, dest="delete_swatches_cb")

    def effect(self):

        svg_copy = deepcopy(self.svg)

        element_list = svg_copy.xpath('//svg:circle | //svg:ellipse | //svg:line | //svg:path | //svg:text | '
                                      '//svg:polygon | //svg:polyline | //svg:rect')

        if len(element_list) < 1:
            inkex.errormsg('No Objects in Document to Process')
            return

        processed_svg = swatch_to_style(self, svg_copy, element_list)

        remove_swatch_defs(self, processed_svg)

        export_svg(self, processed_svg)

if __name__ == '__main__':
    SwatchToStyle().run()
